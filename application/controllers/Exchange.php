<?php
class Exchange extends CI_Controller {
	function __construct(){
		parent:: __construct();
	}

	function index(){

		if($this->input->post('submit')){
			$send_amount = $this->input->post('send_amount');
			$from = 'btc';
			$to = 'eth';
			$address = '';

			$this->session->set_userdata('send_amount',$send_amount);
			$this->session->set_userdata('from',$from);
			$this->session->set_userdata('to',$to);

			
			redirect('exchange/processing');
			
		}
		$this->load->view('exchange/Home_1');
	}

	function login(){
		$username = $this->input->post('email',TRUE);
        $password = $this->input->post('password',TRUE);
        $submit = $this->input->post('submit',TRUE);
        $this->load->library('form_validation');
        if($submit == 'Login')
        {
            $this->form_validation->set_rules('email','email','trim|required');
            $this->form_validation->set_rules('password','password','trim|required');
            if($this->form_validation->run() == True)
            {
                //check if username and password is correct
                $usr_result = $this->_get_user($username, $password);
                $user_data = $usr_result->result();
                
                if ($user_data[0]->id > 0) //active user record is present
                {
                    
                    //set the session variables
                    $sessiondata = array(
                        //get user id here
                        'logged_in'=>true,
                        'user_id'=>$user_data[0]->id
                    );
                    $this->session->set_userdata($sessiondata);
                }
                else
                {
                    $msg = "Invalid username and password!";
                    $value = '<div class="alert alert-danger">' . $msg . '</div>';
                    $this->session->set_flashdata('item', $value);
                    redirect(base_url('exchange/login'));
                }
            }
        }
        $data['flash'] = $this->session->flashdata('item');
        $data['title']='Login';
		$this->load->view('user/Login',$data);
	}

	function signup(){

        $this->load->library('form_validation');
        header("Cache-Control: no cache");
        session_cache_limiter("private_no_expire");
        $submit = $this->input->post('register', TRUE);
        
        if($submit == 'Register')
        {

            $this->form_validation->set_rules('name','Name','required');
            $this->form_validation->set_rules('password','Password','trim|required');
            $this->form_validation->set_rules('confirm_password','Confirm Password','trim|required|matches[password]');
            $this->form_validation->set_rules('email','Email','trim|required|valid_email|is_unique[user.email]');
            if($this->form_validation->run() == TRUE)
            {
            	
             	$name = $this->input->post('name',true);
             	$email = $this->input->post('email',true);
             	$password = $this->input->post('confirm_password',true);
             	$hashed_password = $this->_password($password);
             	$query = $this->_custom_query("INSERT INTO user(name,email,password) VALUES('$name','$email','$hashed_password')");

             	$msg = "User registered successfully";
                $value = '<div class="alert alert-danger">' . $msg . '</div>';
                $this->session->set_flashdata('item', $value);
                redirect(base_url('index.php/exchange/signup'));
            }
        }
        $data['title']='Signup';
        $data['flash'] = $this->session->flashdata('item');
        $this->load->view('user/Register',$data);
	}

	function processing(){
		$amount = $this->session->userdata('send_amount');
		$from = $this->session->userdata('from');
		$to = $this->session->userdata('to');
		$method = 'getExchangeAmount';
		$address = '';

		$reesponse = $this->api_request($method,$from,$to,$amount,$address);
			$response_json = json_decode($reesponse);
		echo $receive_amount = $response_json->result;

		if($this->input->post('submit')){
			$address = $this->input->post('address');
			$this->session->set_userdata('address',$address);
			redirect('exchange/confirmation');
		}

		$this->load->view("exchange/Processing");
	}

	function confirmation(){
		$data['amount'] = $this->session->userdata('send_amount');
		$data['from'] =	$this->session->userdata('from');
		$data['to'] = $this->session->userdata('to');
		$data['address'] = $this->session->userdata('address');
		$this->load->view('exchange/Confirmation',$data);
	}

	function create_transction(){
		$amount = $this->session->userdata('send_amount');
		$from =	$this->session->userdata('from');
		$to = $this->session->userdata('to');
		$address = $this->session->userdata('address');
		$method = 'createTransaction';
		$response = $this->api_request($method,$from,$to,$amount,$address);
		$response_json = json_decode($response);

		echo "send ".$amount .' '.$from. " to ".$response_json->result->payinAddress;
		echo "<br>";
		$data['txn_id'] = $response_json->result->id;

		$data['payin_address'] = $response_json->result->payinAddress;
		$this->load->view('exchange/Create_txn',$data);
		
	}

	function get_exchange_amount(){
		$amount =$this->input->post('amount');
		$method = 'getExchangeAmount';
		$from = 'btc';
		$to = 'eth';
		$address = '';

		$response = $this->api_request($method,$from,$to,$amount,$address);
		$response_json = json_decode($response);
		echo $response_json->result;
	}

	function exchange_amount(){
		$method = 'createTransaction';
		$from = 'btc';
		$to = 'eth';
		$address = '';

		$message = json_encode(
        array('jsonrpc' => '2.0', 'id' => 1, 'method' => 'createTransaction', 'params' => array('from'=>'ltc','to'=>'eth','amount'=>'1','address'=>'0x43de4c54ef7c045f4b6798cf22b76e468dc68ec4'))
    	);
	}


	function api_request($method,$from,$to,$amount,$address){
	    $apiKey = '837fd8d4650b4d76b6dbfa64a72443b4';
	    $apiSecret = '625306acef11f11da805bc8e68f97b3d14977c25d7d158f32bdef70ea4b64525';
	    $apiUrl = 'https://api.changelly.com';
	    $message = json_encode(
	        array('jsonrpc' => '2.0', 'id' => 1, 'method' => $method, 'params' => array('from'=>$from,'to'=>$to,'amount'=>$amount,'address'=>$address))
	    );
	    $sign = hash_hmac('sha512', $message, $apiSecret);
	    $requestHeaders = [
	        'api-key:' . $apiKey,
	        'sign:' . $sign,
	        'Content-type: application/json'
	    ];
	    $ch = curl_init($apiUrl);
	    curl_setopt($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $message);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $requestHeaders);
	    
	    $response = curl_exec($ch);
	    curl_close($ch);
	    return $response;
	}

	 function _get_user($usr, $pwd)
    {
        $pwd = $this->_password($pwd);
        $query = "select * from user where email = '" . $usr . "' and password = '" . $pwd . "'";
        $query = $this->_custom_query($query);
        return $query;
    }

    function _password($password = null)
    {
        $password = hash('sha256',$password.SALT);
        return  $password;
    }

	function _custom_query($query){
		$this->load->model('Exchange_model');
        return $this->Exchange_model->_custom_query($query);
	}
}