<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>
    <meta charset="utf-8">
    <title>Home</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- mobile specific metas
   ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="<?=base_url()?>public/css/lib/bootstrap.mine86f.css?v=2018021514">
    <link rel="stylesheet" href="<?=base_url()?>public/css/lib-stylese86f.css?v=2018021514">
    <!--<link rel="stylesheet" href="/<?=base_url()?>public/css/app.min.css?v=2018021514">-->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>public/css/homepage_v2.mine86f.css?v=2018021514"/>

    <!-- favicons
	================================================== -->
    <!--<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="<?=base_url()?>public/images/favicon_new.png" type="image/x-icon">-->

    <!-- start Mixpanel --><script type="text/javascript">(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,
        0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
    for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);
mixpanel.init("02c8ea110f5d7c6dcf9180c69ebdc9ce");</script><!-- end Mixpanel -->
    <!-- Reddit Conversion Pixel -->
    <script>
        var i=new Image();i.src="../alb.reddit.com/t6130.gif?q=CAAHAAABAAoACQAAAAAQ6xriAA==&amp;s=Z7rm6uk8v_T5QQS3KWVX_GGc-VgXuM36mIh8XxQES58=";
    </script>
    <noscript>
        <img height="1" width="1" style="display:none"
             src="../alb.reddit.com/t6130.gif?q=CAAHAAABAAoACQAAAAAQ6xriAA==&amp;s=Z7rm6uk8v_T5QQS3KWVX_GGc-VgXuM36mIh8XxQES58="/>
    </noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Reddit Conversion Pixel -->
</head>
<body class="font" style="-webkit-font-smoothing: antialiased;">
<script>
    window.is_inactive = true;
    window.is_prod = '1';
    if(is_prod == '1') {
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '../www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-100639244-1', 'auto');
        ga('send', 'pageview', "/home" + window.location.pathname);
//        ga('send', 'pageview');
        <!-- Facebook Pixel Code -->
        !function (f, b, e, v, n, t, s) {
            if (f.fbq)return;
            n = f.fbq = function () {
                n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq)f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window,
                document, 'script', '../connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1570069006632609', {
            em: 'care@coinswitch.co'
        });
        fbq('track', 'PageView');

        // mix panel
        if (window.mixpanel) {
            mixpanel.track("Page_View");
        }

        window.fbAsyncInit = function() {
            FB.init({
                appId      : '1490579831058785',
                xfbml      : true,
                version    : 'v2.10'
            });
            FB.AppEvents.logPageView();
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "../connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    }
</script>
<noscript><img height="1" width="1" style="display:none"
               src="https://www.facebook.com/tr?id=1570069006632609&amp;ev=PageView&amp;noscript=1"
/></noscript>
<div style="background-image: url('http://eskipaper.com/images/hot-air-balloon-wallpaper-67.jpg');background-position: left;
    background-repeat: no-repeat;
    background-size: cover;
    height: auto;
    width: auto;">
   
    <div style="background-color: black">
        <div class="nav-border position-fixed">
            <nav class="navbar navbar-inverse">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"
                                style="float: left;margin-left: 10px;">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <!-- <a href="index.html"><img class="navbar-brand nav-image" src="<?=base_url()?>public/images/Logo.svg" ></a> -->
                        <h3>LOGO</h3>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav navbar-right" style="margin:7.5px -15px;">
                            <!--<li class="width  font-OS-Reg"><a href="#">Track Order</a></li>-->
                            <li class="width font-OS-Reg"><a  href="index.html">HOME</a></li>
                            
                            <li class="width  font-OS-Reg"><a class="smoothscroll" href="#">FAQ</a></li>
                            <li class="width  font-OS-Reg"><a href="signin.html">LOGIN</a></li>
                            <li class="width  font-OS-Reg"><a href="signup.html">SIGN UP</a></li>
                            
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <div class="container hidden" style="padding-top: 50px">
            <div class="row">
                <div class="col-lg-1 col-md-1 col-sm-1 coin-inc-dec  font-OS-Reg col-padding-5">
                    <h4 class="font-bold">BTC <span class="red">-3.08%</span></h4>
                    <h4 class="font-14">$13,466.40</h4>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 coin-inc-dec  font-OS-Reg margin-left-20 col-padding-5">
                    <h4 class="font-bold">XRP <span class="green">1.81% </span></h4>
                    <h4 class="font-14">$1.97</h4>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 coin-inc-dec  font-OS-Reg margin-left-20 col-padding-5">
                    <h4 class="font-bold">ETH <span class="green">14.39% </span></h4>
                    <h4 class="font-14">$38.36</h4>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 coin-inc-dec  font-OS-Reg margin-left-20 col-padding-5">
                    <h4 class="font-bold">BCH <span class="red">-2.6% </span></h4>
                    <h4 class="font-14">$2,307.20</h4>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 coin-inc-dec  font-OS-Reg margin-left-20 col-padding-5">
                    <h4 class="font-bold">LTC <span class="green">5.05%</span></h4>
                    <h4 class="font-14">$236.11</h4>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 coin-inc-dec font font-OS-Reg margin-left-20 col-padding-5">
                    <h4 class="font-bold">XEM <span class="green">2.53%</span></h4>
                    <h4 class="font-14">$1.07</h4>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 coin-inc-dec  font-OS-Reg margin-left-20 col-padding-5">
                    <h4 class="font-bold">DASH <span class="green">6.3%</span></h4>
                    <h4 class="font-14">$1,063</h4>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 coin-inc-dec  font-OS-Reg margin-left-20 col-padding-5">
                    <h4 class="font-bold">XMR <span class="green">6.06%</span></h4>
                    <h4 class="font-14">$341.98</h4>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 coin-inc-dec  font-OS-Reg margin-left-20 col-padding-5">
                    <h4 class="font-bold">NEO <span class="green">13.37% </span></h4>
                    <h4 class="font-14">$86.47</h4>
                </div>
                <div class="col-lg-1 col-md-1 col-sm-1 coin-inc-dec font-OS-Reg margin-left-20 col-padding-5">
                    <h4 class="font-bold">ETC <span class="green">0.3% </span></h4>
                    <h4 class="font-14">$29.60</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="container padding">
        <div class="heading-padding">
            <section>
                <h1 class="top-heading  font-OS-Bold">Exchange
                    <div class="slidingVertical">
                        <span>Cryptocurrency</span>
                        <span>Bitcoin</span>
                        <span>Litecoin</span>
                        <span>Ethereum</span>
                        <span>Altcoins</span>
                    </div>
                </h1>
                <h1 class="top-heading  font-OS-Bold margin-top-0">at the best rate.</h1>
            </section>
            <p class="side-text  font-OS-Reg">One place to convert your cryptocurrencies across
                all exchanges at the best rates.</p>
        </div>
    </div>
    <div class="converter-bg">
        <div class="container">
            <form class="form-inline form-exchange" action ="<?=base_url('index.php/exchange');?>" method="post">

                <input class="form-control" type="text" name="from_currency" autocomplete="off" style="background: whitesmoke;display:none">
                <input class="form-control" type="text" name="to_currency" autocomplete="off" style="background: whitesmoke;display:none">

                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <h5 class="converter-text  font-OS-Bold margin-bottom">You Send <span class="you-send-message">Bitcoin</span></h5>
                        <div class="converter-flex">
                            <input class="input-text width-60p  font-OS-Bold selected_currency_amount" id="send_amount" type="number" step="any" name="send_amount" autocomplete="off">
                            <div class="font-OS-Bold dropdown ">
                                <button type="button" class="dropbtn input-text dropdown-toggle" data-toggle="dropdown">
                                    <img src='../s3.ap-south-1.amazonaws.com/crypto-exchange/coins-sm/bitcoin.png' class="select-img default_from_img">
                                    <b class="selected_currency" data-symbol='btc'>BTC</b>
                                    <img src="<?=base_url()?>public/images/dropdown-img.png" class="dropdown-img">
                                </button>
                                <ul class="dropdown-menu currency-dropdown-menu dropdown-menu-right" style="z-index:10;">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="basic-addon1"><i class="fa fa-search"></i></span>
                                        <input autocomplete="off" type="text" class="form-control fix-position-input" placeholder="Search" id="fromFilterCurrency" style="border:none;">
                                    </div>
                                    <div class="dropdown-list-wrapper dropdown-menu-from dropdown-menu-right">
                                    </div>

                                </ul>
                            </div>
                        </div>
                    </div> 

                     <div class="col-xs-12 col-sm-1 col-md-1 col-lg-1">
                        <div class="converter-img-div">
                            <img src="<?=base_url()?>public/images/exchange.svg" class="converter-img img-change" style="cursor: pointer;width: 40px; height: 40px">
                        </div>
                    </div> 


                     <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <h5 class="converter-text margin-bottom  font-OS-Bold">You Get <span class="you-get-message">Ethereum</span></h5>
                        <div class="converter-flex">
                            <input id="get_amount" type="text" class="input-text width-60p font-OS-Bold" readonly="" style="background-color:#fff78d;">
                            <div class="font-OS-Bold dropdown ">
                                <button type="button" class="dropbtn input-text dropdown-toggle" data-toggle="dropdown">
                                    <img src='../s3.ap-south-1.amazonaws.com/crypto-exchange/coins-sm/ether.png' class="select-img default_to_img">
                                    <b class="conversion_currency" data-symbol='eth'>ETH</b>
                                    <img src="<?=base_url()?>public/images/dropdown-img.png" class="dropdown-img">
                                </button>
                                <ul class="dropdown-menu currency-dropdown-menu dropdown-menu-right" style="z-index:10;">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                        <input autocomplete="off" type="text" class="form-control fix-position-input" placeholder="Search" id="toFilterCurrency" style="border:none;">
                                    </div>
                                    <div class="dropdown-list-wrapper dropdown-menu-to dropdown-menu-right">
                                    </div>
                                </ul>
                            </div>
                        </div> 

                        <div class="converter-flex visible-xsFlex ">
                            <input type="text" class="input-text width-60p font-OS-Reg">
                            <div class="font-OS-Bold dropdown ">
                                <button type="button" class="dropbtn input-text">
                                    <img src='https://s3.ap-south-1.amazonaws.com/crypto-exchange/coins-sm/ether.png' class="select-img default_to_img">
                                    <b class="conversion_currency" data-symbol='eth'>ETH</b>
                                    <img src="/<?=base_url()?>public/images/dropdown-img.png" class="dropdown-img">
                                </button>
                                <div id="myDropdown2" class="dropdown-content">
                                    <ul>-->
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                            <input autocomplete="off" type="text" class="form-control fix-position-input" placeholder="Search" id="toFilterCurrency">
                                        </div>
                                        <div class="dropdown-list-wrapper dropdown-menu-to dropdown-menu-right">
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> 

                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 offer-btn-margin"
                         style="display: flex;flex-direction: column">
                        <h5 class="converter-text margin-bottom  font-OS-Bold number-of-offers offer-padding"></h5>
                        <input type="submit" class="btn offer-btn continue-button font-OS-Bold" name="submit" value="Exchange" style="font-size:16px;">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<div class="container" id="home-page-warning">
    <!--<div class="note font-OS-Reg">-->
        <!--<p style="margin: 0px;" id="home-page-warning"></p>-->
    <!--</div>-->
</div>

<div style="padding-top: 50px;">
    <div class="container padding">
        <div id="offering-div">
            <div class="row   ">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-padding-15 margin-bottom-20">
                    <h3 class="offering heading font-OS-Bold">Widest Offering</h3>

                    <p class="offering font-OS-Reg">Exchange over 250 cryptocurrencies.
                        <a target="_blank" href="#"><span class="color-blue" > View all supported coins.</span></a>
                    </p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-padding-15 margin-bottom-20">
                    <h3 class="offering heading font-OS-Bold">Interchangeability</h3>

                    <p class="offering font-OS-Reg">Exchange between 6000+ possible cryptocurrency pairs.</span></p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-padding-15 margin-bottom-20">
                    <h3 class="offering heading font-OS-Bold">Unified Experience</h3>

                    <p class="offering font-OS-Reg">One place to access all exchanges and transact with ease.
                        <!--<span class="color-blue"> track your orders.</span>-->
                    </p>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 col-padding-15 margin-bottom-20">
                    <h3 class="offering heading font-OS-Bold">Reliable Support</h3>

                    <p class="offering font-OS-Reg">Our team will help you with all transactions across any of our
                        supported
                        exchanges.</span></p>
                </div>
            </div>
        </div>
        <div class="outer-div  ">
            <h2 class="side-heading font-OS-Bold">How It Works</h2>

            <div class="row align-center-left">
                <div class="col-xs-12  col-sm-4 col-md-4 col-lg-4 display-center margin-bottom-20">
                    <div class="row">
                        <div class="col-xs-3 col-sm-12 col-md-12 col-lg-12 padding-bottom-20">
                            <img src="<?=base_url()?>public/images/STOCKS.svg" class="work-img">
                        </div>
                        <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12">
                            <h3 class="work-title font-OS-Bold">1.Compare </h3>

                            <p class="work-text font-OS-Reg">Compare rates across exchanges and find the best exchange rate.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12  col-sm-4 col-md-4 col-lg-4 display-center margin-bottom-20">
                    <div class="row">
                        <div class="col-xs-3 col-sm-12 col-md-12 col-lg-12 padding-bottom-20">
                            <img src="<?=base_url()?>public/images/ALTCOINS.svg" class="work-img">
                        </div>
                        <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12">
                            <h3 class="work-title font-OS-Bold">2.Convert</h3>

                            <p class="work-text font-OS-Reg">Follow simple steps to exchange your cryptocurrency. Need help? <a href="#">Refer our guide.</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 display-center margin-bottom-20">
                    <div class="row">
                        <div class="col-xs-3 col-sm-12 col-md-12 col-lg-12 padding-bottom-20">
                            <img src="<?=base_url()?>public/images/ORDERED%20RECORDS.svg" class="work-img">
                        </div>
                        <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12">
                            <h3 class="work-title font-OS-Bold">3.Track</h3>

                            <p class="work-text font-OS-Reg">Easily <a href="">track your order</a> progress with our simplified and intuitive UI.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="outer-div align-left hidden">
            <h2 class="side-heading font-OS-Bold">Latest transactions on CoinSwitch</h2>

            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 margin-bottom-40">
                    <div class="row">
                        <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12 width-65p">
                            <h3 class="latest-sec align-left font-OS-Reg">30 Seconds ago</h3>

                            <h3 class="lt-heading font-OS-Bold margin-top-5">0.003 BTC ⇢ ETH</h3>
                        </div>
                        <div class="col-xs-3 col-sm-12 col-lg-12 width-35p">
                            <h5 class="transaction-via font-OS-Reg margin-top-5">via Changelly</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 margin-bottom-40">
                    <div class="row">
                        <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12 width-65p">
                            <h3 class="latest-sec align-left font-OS-Reg">50 Seconds ago</h3>

                            <h3 class="lt-heading font-OS-Bold margin-top-5">0.06 BTC ⇢ LTE</h3>

                        </div>
                        <div class="col-xs-3 col-sm-12 col-md-12 col-lg-12 width-35p">
                            <h5 class="transaction-via font-OS-Reg margin-top-5">via Shapeshift</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 margin-bottom-40">
                    <div class="row">
                        <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12 width-65p">
                            <h3 class="latest-sec align-left font-OS-Reg">1 Minute ago</h3>

                            <h3 class="lt-heading font-OS-Bold margin-top-5">0.65 ETH ⇢ LTE</h3>
                        </div>
                        <div class="col-xs-3 col-sm-12 col-md-12 col-lg-12 width-35p">
                            <h5 class="transaction-via font-OS-Reg margin-top-5">via Blocktrade</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 margin-bottom-40">
                    <div class="row">
                        <div class="col-xs-9 col-sm-12 col-md-12 col-lg-12 width-65p">
                            <h3 class="latest-sec align-left font-OS-Reg">30 Seconds ago</h3>

                            <h3 class="lt-heading font-OS-Bold margin-top-5">9 DOGE ⇢ ETH</h3>
                        </div>
                        <div class="col-xs-3 col-sm-12 col-md-12 col-lg-12 width-35p">
                            <h5 class="transaction-via font-OS-Reg margin-top-5">via Changelly</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row hidden-xs">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" class="margin-bottom-20">
                    <h3 class="latest-sec align-left font-OS-Reg">30 Seconds ago</h3>

                    <h3 class="lt-heading font-OS-Bold margin-top-5">0.003 BTC ⇢ ETH</h3>
                    <h5 class="transaction-via font-OS-Reg margin-top-5">via Changelly</h5>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" class="margin-bottom-20">
                    <h3 class="latest-sec align-left font-OS-Reg ">50 Seconds ago</h3>

                    <h3 class="lt-heading font-OS-Bold margin-top-5">0.06 BTC ⇢ LTE</h3>
                    <h5 class="transaction-via font-OS-Reg margin-top-5">via Shapeshift</h5>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" class="margin-bottom-20">
                    <h3 class="latest-sec align-left font-OS-Reg">1 Minute ago</h3>

                    <h3 class="lt-heading font-OS-Bold margin-top-5">0.65 ETH ⇢ LTE</h3>
                    <h5 class="transaction-via font-OS-Reg margin-top-5">via Blocktrade</h5>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" class="margin-bottom-20">
                    <h3 class="latest-sec align-left font-OS-Reg">30 Seconds ago</h3>

                    <h3 class="lt-heading font-OS-Bold margin-top-5">9 DOGE ⇢ ETH</h3>
                    <h5 class="transaction-via font-OS-Reg margin-top-5">via Changelly</h5>
                </div>
            </div>
        </div>
        <div class="outer-div  align-left">
            <h2 class="side-heading font-OS-Bold">Testimonials</h2>

            <div id="myCarousel" class="carousel slide" data-ride="carousel">

                <!-- Wrapper for slides -->
                <div class="carousel-inner">
                    <div class="item active">
                        <p class="carousel-text font-OS-Reg">“A great concept. The process is easy to understand and use. I had an issue, which wasn't their fault, and I was able chat with customer service immediately and get the issue resolved. I highly recommend.”</p>
                        <h3 class="carousel-author font-OS-Bold">Andrew Davis</h3>
                        <h4 class="carousel-date hidden font-OS-Reg">BTC to ETH, 21 Dec 2017</h4>
                    </div>

                    <div class="item">
                        <p class="carousel-text font-OS-Reg">“Super fast experience. Supports all major cryptocurrencies and the entire transaction flow is super smooth. Highly recommended!”</p>
                        <h3 class="carousel-author font-OS-Bold">Archit Rathi</h3>
                        <h4 class="carousel-date hidden font-OS-Reg">BTC to ETH, 21 Dec 2017</h4>
                    </div>

                    <div class="item">
                        <p class="carousel-text font-OS-Reg">“Trading was straight forward and support were very fast and helpful.”</p>
                        <h3 class="carousel-author font-OS-Bold">Jack Pearson</h3>
                        <h4 class="carousel-date hidden font-OS-Reg">BTC to ETH, 21 Dec 2017</h4>
                    </div>

                    <!--<div class="item">-->
                        <!--<p class="carousel-text font-OS-Reg">“I cannot imagine a simpler and more intuitive way to-->
                            <!--exchange-->
                            <!--crypto!”</p>-->

                        <!--<h3 class="carousel-author font-OS-Bold">Alejandro K</h3>-->
                        <!--<h4 class="carousel-date hidden font-OS-Reg">BTC to ETH, 21 Dec 2017</h4>-->
                    <!--</div>-->

                    <!--<div class="item">-->
                        <!--<p class="carousel-text font-OS-Reg">“I cannot imagine a simpler and more intuitive way to-->
                            <!--exchange-->
                            <!--crypto!”</p>-->

                        <!--<h3 class="carousel-author font-OS-Bold">Alejandro K</h3>-->
                        <!--<h4 class="carousel-date hidden font-OS-Reg">BTC to ETH, 21 Dec 2017</h4>-->
                    <!--</div>-->

                </div>

                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <!--<li data-target="#myCarousel" data-slide-to="3"></li>-->
                    <!--<li data-target="#myCarousel" data-slide-to="4"></li>-->
                </ol>
            </div>
        </div>
        <div class="section-scroll faq-support align-left accordion-link" id="faq">
            <h2 class="side-heading font-OS-Bold">FAQ & Support </h2>

            <div class="hidden-xs">
                <div class="row faq-row-margin">
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        <img src="<?=base_url()?>public/images/OTC%20TRADING.svg" class="faq-img">

                    </div>
                    <div class="col-sm-10 col-md-10 col-lg-10">
                        <h3 class="accordion-title font-OS-Bold">About CoinSwitch</h3>

                        <div class="faq-para font-OS-Reg">
                            <div class="panel-group" id="desktopaccordion1">
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="What-is-CoinSwitch" data-toggle="collapse" data-parent="#desktop-accordion1"  href="#desktopcollapseInnerOne">
                                                <p class="margin-bottom-0">What is CoinSwitch?</p>
                                            </a></h4>
                                    </div>
                                    <div id="desktopcollapseInnerOne" class="panel-collapse collapse">
                                        <div class="panel-body border-top-0  inner-accord-padding font-italic">
                                            CoinSwitch is a cryptocurrency exchange providing the best exchange rate on transactions by aggregating all leading exchanges and comparing their rates in real time. We support over <b>250 cryptocurrencies</b> and more than <b>6000 pairs</b> for exchange.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="Why-should-I-trust-CoinSwitch" data-toggle="collapse" data-parent="#desktopaccordion1" href="#desktopcollapseInnerTwo">
                                            <p class="margin-bottom-0">Why should I trust you?</p>
                                        </a></h4>
                                    </div>
                                    <div id="desktopcollapseInnerTwo" class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding font-italic">
                                            CoinSwitch is built on the most trusted exchanges in the world. We provide a seamless experience by integrating with reliable and secure exchanges and helping our customers get the best rates while ensuring their security.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="Can-the-rates-change-during-the-transaction" data-toggle="collapse" data-parent="#desktopaccordion1" href="#desktopcollapseInnerThree">
                                            <p class="margin-bottom-0">Can the rates change during the transaction?</p>
                                        </a></h4>
                                    </div>
                                    <div id="desktopcollapseInnerThree" class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding font-italic">
                                            Unlike ordinary money, cryptocurrencies are highly volatile, so the rate may fluctuate. It may happen that the exchange rate which you see before the transaction is different (high or low) after transaction gets completed. In that case, the exchanged quantity which you receive may differ from what you see during the comparison.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="What-is-CoinSwitch-transaction-fees" data-toggle="collapse"  data-parent="#desktopaccordion1"  href="#desktopcollapseInnerFour">
                                            <p class="margin-bottom-0">What is our transaction fees?</p>
                                        </a></h4>
                                    </div>
                                    <div id="desktopcollapseInnerFour" class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding font-italic">
                                            <p>
                                                Our fees depends on the exchange you select for your transaction. The conversion rate you see is inclusive of the fees.
                                                <br/><strong>Note:</strong> Below fees does not include any amount charged by the exchange itself.
                                            </p>
                                            <br/>
                                            <div class="row" style="padding:0px;">
                                                <div class="col-lg-4  col-sm-6 col-md-4 col-xs-12 col-lg-offset-4 col-sm-offset-3 col-md-offset-4" style="padding:0px;">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th>Exchange</th>
                                                            <th>Fees</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>Changelly</td>
                                                            <td>0%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Shapeshift</td>
                                                            <td>0%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>EverCoin</td>
                                                            <td>0%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Blocktrades</td>
                                                            <td>0%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Bittrex</td>
                                                            <td>0.49%-0.98%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Cryptopia</td>
                                                            <td>0.49%-0.98%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Others</td>
                                                            <td>0.49%-0.98%</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row faq-row-margin">
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        <img src="<?=base_url()?>public/images/BITCOIN%20WALLET.svg" class="faq-img">
                    </div>
                    <div class="col-sm-10 col-md-10 col-lg-10">
                        <h3 class="accordion-title font-OS-Bold">About Wallets</h3>

                        <div class="faq-para font-OS-Reg">
                            <div class="panel-group" id="desktopaccordion2">
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="What-is-the-wallet-address" data-toggle="collapse" data-parent="#accordion2"
                                               href="#desktopsecondInnerOne" class="click-highlight">
                                                <p class="margin-bottom-0">What’s the wallet address?</p>
                                            </a></h4>
                                    </div>
                                    <div id="desktopsecondInnerOne" class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding">
                                            A wallet is a digital place for your coins to be stored. Each coin has a certain wallet provider. That could be official GUI-wallets, web wallets and other applications. The wallet address represents a randomly generated combination of digits and letters. CoinSwitch doesn’t provide wallet addresses and never stores user deposits. To exchange cryptocurrencies, you need to have a wallet address.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="How-do-I-get-a-wallet-address" data-toggle="collapse"
                                                                   data-parent="#desktopaccordion2"
                                                                   href="#desktopsecondInnerTwo"
                                                                   class="click-highlight">
                                            <p class="margin-bottom-0"> How do I get a wallet address?</p>
                                        </a></h4>
                                    </div>
                                    <div id="desktopsecondInnerTwo" class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding">
                                            Just figure out, what coin do you want to buy and seek for a stable wallet version. As a rule, each coin has its official wallet client. Note that each wallet has its unique address or a tag with a private key that is required to restore your wallet if lost. CoinSwitch never asks your private keys. Store them in a safe place and never show anyone. Once private keys are stolen, your wallet with all the coins will be lost forever.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="What-is-the-recipient-address" data-toggle="collapse"
                                                                   data-parent="#desktopaccordion2"
                                                                   href="#desktopsecondInnerThree">
                                            <p class="margin-bottom-0"> What's the recipient’s address?</p>
                                        </a></h4>
                                    </div>
                                    <div id="desktopsecondInnerThree" class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding">
                                            In each transaction, there is always a sender and a recipient. The recipient’s address is the wallet address to receive the money you want to buy. For example, if you want to buy Ethereum (ETH), in the field, you should specify the ETH wallet address. In general, the recipient’s wallet address is the address we send coins bought, once a transaction is finished.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="What-is-a-hash" data-toggle="collapse"
                                                                   data-parent="#desktopaccordion2"
                                                                   href="#desktopsecondInnerFour">
                                            <p class="margin-bottom-0"> What is a hash?</p>
                                        </a></h4>
                                    </div>
                                    <div id="desktopsecondInnerFour" class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding">
                                            A hash (or the tx/transaction ID) is a unique address of your transaction in a blockchain. Mostly, the hash is a combination of digits and lower case (upper in Ripple) letters that represents a proof that money is sent. Whenever you make a payment, you receive a hash displayed in your wallet. Hash should be located in a blockchain. Our support team may ask you to provide a hash. Sometimes it’s required to find your payment and resolve your issues if any.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="Why-is-my-wallet-address-recognized-as-invalid" data-toggle="collapse"
                                                                   data-parent="#desktopaccordion2"
                                                                   href="#desktopsecondInnerFive">
                                            <p class="margin-bottom-0">Why is my wallet address recognized as invalid?</p>
                                        </a></h4>
                                    </div>
                                    <div id="desktopsecondInnerFive" class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding">
                                            CoinSwitch supports most of the wallets. If your wallet address is recognized as invalid, there is always a reason: You confused Dash (DASH) with Dashcoin DSH). These are two different coins with two different amounts. You confused Factom (FCT) with Fantomcoin (FCN). Different currencies as well. Typo or character missing. Make sure that the address you specify matches the actual address of your wallet.
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row faq-row-margin">
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        <img src="<?=base_url()?>public/images/BLOCKCHAIN.svg" class="faq-img">

                    </div>
                    <div class="col-sm-10 col-md-10 col-lg-10">
                        <h3 class="accordion-title font-OS-Bold">About Blockchains</h3>

                        <div class="faq-para font-OS-Reg">
                            <div class="panel-group" id="desktopaccordion3">
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id=">What-is-a-memo" data-toggle="collapse" data-parent="#desktopaccordion3"
                                               href="#desktopThirdInnerOne">
                                                <p class="margin-bottom-0">What is a memo?</p>
                                            </a></h4>
                                    </div>
                                    <div id="desktopThirdInnerOne" class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding">
                                            <p>   <p>A memo is a combination of digits and letters required to assign STEEM to a certain Steem
                                            wallet user.
                                            Memos are used in Steem wallets and indicated right here.</p>
                                            <p>In the wallet, a memo should look like this</p><img
                                                src="../s3-ap-south-1.amazonaws.com/crypto-exchange/placeholder/image3.png"/>
                                            <p>If you want to buy Steems, make sure that you've specified a correct memo while putting in a
                                                wallet
                                                address.</p>
                                            <p>Note that you have a nickname on steemit.com, you may use it as your wallet address that
                                                doesn't require
                                                a memo.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="What-is-a-destination-tag" data-toggle="collapse"
                                                                   data-parent="#desktopaccordion3"
                                                                   href="#desktopThirdInnerTwo">
                                            <p class="margin-bottom-0">What is a destination tag?</p>
                                        </a></h4>
                                    </div>
                                    <div id="desktopThirdInnerTwo" class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding">
                                            <p>A destination tag is a short complementary combination of characters required for certain
                                                Ripple wallets.
                                                While buying Ripple, make sure that you have specified a destination tag if required.
                                                Likewise, when you
                                                send us Ripples, don't forget about a destination tag provided near the address to send
                                                to.</p><img
                                                src="../s3-ap-south-1.amazonaws.com/crypto-exchange/placeholder/image2.png"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="What-is-a-message" data-toggle="collapse"
                                                                   data-parent="#desktopaccordion3"
                                                                   href="#desktopThirdInnerThree">
                                            <p class="margin-bottom-0">What is a message?</p>
                                        </a></h4>
                                    </div>
                                    <div id="desktopThirdInnerThree" class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding">
                                            <p>The message is an additional tag required for XEM transaction. Whenever you send or receive XEM,
                                                make sure
                                                that you've input a message. You may find it in your wallet, and it should look like
                                                this.</p><img
                                                src="../s3-ap-south-1.amazonaws.com/crypto-exchange/placeholder/image1.png"/>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row faq-row-margin hidden-xs">
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        <img src="<?=base_url()?>public/images/DECENTRALIZED_2.svg" class="faq-img">

                    </div>
                    <div class="col-sm-10 col-md-10 col-lg-10">
                        <h3 class="accordion-title font-OS-Bold">About Exchanging Cryptocurrency and altcoins</h3>

                        <div class="faq-para font-OS-Reg">
                            <!-- Here we insert another nested accordion -->
                            <div class="panel-group" id="desktopaccordion4">
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="How-do-I-exchange-coins-on-CoinSwitch" data-toggle="collapse" data-parent="#desktopaccordion4"
                                               href="#desktopFourthInnerOne">
                                                <p class="margin-bottom-0">How do I exchange coins on CoinSwitch?</p>
                                            </a></h4>
                                    </div>
                                    <div id="desktopFourthInnerOne" class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding">

                                            <p>For all available coins on CoinSwitch, the exchange process is similar. Click <a href="https://blog.coinswitch.co/coinswitch-exchange-tutorial-274acaca10a9" target="_blank">here</a> to view our exchange guide.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="What-is-the-minimum-maximum-amount" data-toggle="collapse"
                                                                   data-parent="#desktopaccordion4"
                                                                   href="#desktopFourthInnerTwo">
                                            <p class="margin-bottom-0">What is the minimum/maximum amount?</p>
                                        </a></h4>
                                    </div>
                                    <div id="desktopFourthInnerTwo" class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding">
                                            <p>CoinSwitch doesn’t have amount restrictions. However, there are some nuances.</p>
                                            <p>If an amount is too low, make sure that it will cover all the <a
                                                    href="#what-are-network-fees">network
                                                fees</a> a blockchain takes. It means that the amount you are going to send and the amount
                                                you are going
                                                to get should be enough to cover input and output network fees taken by a blockchain.</p>
                                            <p>If your funds to be exchanged are insufficient, you will see a message. <p/>
                                            <p>Please stick with this recommendations. Otherwise, there is a high risk that your money will
                                                be lost.</p>
                                            <p>Some exchanges provide a max limit as well.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="What-are-the-network-fees" data-toggle="collapse"
                                                                   data-parent="#desktopaccordion4"
                                                                   href="#desktopFourthInnerThree">
                                            <p class="margin-bottom-0">What are the network fees?</p>
                                        </a></h4>
                                    </div>
                                    <div id="desktopFourthInnerThree" class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding">
                                            Every cryptocurrency is based on a blockchain. The process requires a network fee.
                                            This is a commission a blockchain takes from the amount you and we send. This is why it is
                                            so important to include network fees in the amount you send and going to get. If the amount
                                            is too low to cover the fees, a transaction will fail.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="When-should-I-receive-my-money" data-toggle="collapse"
                                                                   data-parent="#desktopaccordion4"
                                                                   href="#desktopFourthInnerFour">
                                            <p class="margin-bottom-0">When should I receive my money?</p>
                                        </a></h4>
                                    </div>
                                    <div id="desktopFourthInnerFour" class="panel-collapse collapse ">
                                        <div class="panel-body border-top-0 inner-accord-padding">
                                            CoinSwitch transactions take 15-30 minutes to be processed. If a transaction is large (around 1 BTC worth), processing may take longer depending on the size of your transaction and blockchain capacity.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Inner accordion ends here -->
                        </div>
                    </div>
                </div>

                <div class="row faq-row-margin hidden-xs">
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        <img src="<?=base_url()?>public/images/TIMESTAMP.svg" class="faq-img">

                    </div>
                    <div class="col-sm-10 col-md-10 col-lg-10">
                        <h3 class="accordion-title font-OS-Bold">About Transactions</h3>

                        <div class="faq-para font-OS-Reg">
                            <!-- Here we insert another nested accordion -->
                            <div class="panel-group" id="desktopaccordion5">
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="How-do-I-cancel-my-transaction" data-toggle="collapse" data-parent="#desktopaccordion5"
                                               href="#desktopFifthInnerOne">
                                                <p class="margin-bottom-0">How do I cancel my transaction?</p>
                                            </a></h4>
                                    </div>
                                    <div id="desktopFifthInnerOne" class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding">
                                            Blockchain transactions are irreversible. Once the money is sent, it cannot be rolled back. So if you are going to exchange cryptos, think twice and check attentively all the payment details prior to sending money.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="Why-does-my-transaction-take-so-long" data-toggle="collapse"
                                                                   data-parent="#desktopaccordion5"
                                                                   href="#desktopFifthInnerTwo">
                                            <p class="margin-bottom-0">Why does my transaction take so long?</p>
                                        </a></h4>
                                    </div>
                                    <div id="desktopFifthInnerTwo" class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding">
                                            <p>As a rule, our transaction takes 15-30 minutes to be processed. If your transaction takes
                                                longer, this might be due to a wide range of factors.
                                            </p>
                                            <ol>
                                                <li>Blockchain overloaded. There are many transactions pending including yours. These issues take
                                                    place on blockchains. All you have to do is to wait. CoinSwitch doesn't control blockchain delays.
                                                </li>
                                                <li>DDoS attacks. Any platform may experience such issues. In this case, all you have to do is
                                                    wait.</li>
                                            </ol>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="Why-is-my-transaction-still-waiting-for-payment" data-toggle="collapse"
                                                                   data-parent="#desktopaccordion5"
                                                                   href="#desktopFifthInnerThree">
                                            <p class="margin-bottom-0">Why is my transaction still waiting for payment
                                                if I've already
                                                paid?</p>
                                        </a></h4>
                                    </div>
                                    <div id="desktopFifthInnerThree"
                                         class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding">
                                            <p>This might be due to several reasons.</p>
                                            <ul style="padding-left: 15px;">
                                                <li>The transaction hasn't been included to a blockchain. Cryptocurrencies aren't stable, so
                                                    minor errors
                                                    might occur. Either can we refund money or push a payment through if you provide the <a
                                                            href="#">hash </a>(a tx ID) of your transaction.
                                                </li>
                                                <li>ETC and ETH confusion. The addresses of Ethereum (ETH) and Ethereum Classic (ETH) are of
                                                    the same
                                                    structure. If you send ETC or ETH, make sure that you've created an appropriate
                                                    transaction on
                                                    CoinSwitch. For example, if you create an ETH to BTC transaction, make sure that you
                                                    send ETH, not
                                                    ETC. Otherwise, your transaction will be stuck.
                                                </li>
                                                <li>Wrong XEM <a href="#what-is-message">message</a>. While sending XEM, make sure that
                                                    you've put a
                                                    correct message. It's indicated here and looks like a combination of digits and letters.
                                                    Messages
                                                    like 'Hey how are you', 'I love CoinSwitch' etc. are lovely but don't work,
                                                    unfortunately :)
                                                </li>
                                                <li>Other internal errors. Even our perfect system may lag or experience internal issues. If
                                                    you suppose
                                                    that this is the case, report it to <a href="cdn-cgi/l/email-protection.html" class="__cf_email__" data-cfemail="98ebede8e8f7eaecd8fbf7f1f6ebeff1ecfbf0b6fbf7">[email&#160;protected]</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="What-will-happen-if-I-close-the-browser-tab" data-toggle="collapse"
                                                                   data-parent="#desktopaccordion5"
                                                                   href="#desktopFifthInnerFour">
                                            <p class="margin-bottom-0">What will happen if I close the browser tab?</p>
                                        </a></h4>
                                    </div>
                                    <div id="desktopFifthInnerFour"
                                         class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding">
                                            <p>If you have transferred the amount, then you can surely close the browser tab. Our system will automatically complete the transaction, and the amount will be deposited into your recipient wallet address. </br>Just keep the the <b>order-id</b> with you for any follow ups.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="Why-blockchain-verification-is-taking-long" data-toggle="collapse"
                                                                   data-parent="#desktopaccordion5"
                                                                   href="#desktopFifthInnerFive">
                                            <p class="margin-bottom-0">Why blockchain verification is taking long?</p>
                                        </a></h4>
                                    </div>
                                    <div id="desktopFifthInnerFive"
                                         class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding">
                                            Blockchain is peer to peer network where transactions need to be confirmed by the network. This process can take from few minutes to several hours depending on the cryptocurrency and load on the network. <br/>
                                            CoinSwitch or its partners don't play any role in blockchain confirmations. Please be patient in case of delay. Your fund is safe and the exchange will proceed once our partner receives the deposits.
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default border-top-0 box-shadow-0">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a class="accordion-link" id="Sent-funds-to-the-wrong-address" data-toggle="collapse"
                                                                   data-parent="#desktopaccordion5"
                                                                   href="#desktopFifthInnerSix">
                                            <p class="margin-bottom-0">Sent funds to the wrong address. Can I recover my
                                                funds?</p>
                                        </a></h4>
                                    </div>
                                    <div id="desktopFifthInnerSix" class="panel-collapse collapse">
                                        <div class="panel-body border-top-0 inner-accord-padding">
                                            Unfortunately not. Once funds are sent to a wrong address, there is nothing we can do to recover them. This is why it is extremely important to double check the address you enter while ordering.
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Inner accordion ends here -->
                        </div>
                    </div>
                </div>

                <div class="row hidden-xs" style="padding-bottom: 20px;">
                    <div class="col-sm-2 col-md-2 col-lg-2">
                        <img src="<?=base_url()?>public/images/PEER-TO-PEER.svg" class="faq-img">
                    </div>
                    <div class="col-sm-10 col-md-10 col-lg-10">
                        <h3 class="font-OS-Bold" style="font-size: 24px, color: #444444">Your issue not listed
                            here?</h3>

                        <p class="font-OS-Reg">If you have a problem or a question about our service, you can reach us
                            at
                            <b class="font-OS-Bold"><a href="cdn-cgi/l/email-protection.html" class="__cf_email__" data-cfemail="5b282e2b2b34292f1b38343235282c322f3833753834">[email&#160;protected]</a></b><br/>
                            We usually respond to email requests within 12 hours.</p>

                    </div>
                </div>
            </div>

            <div class="visible-xs font-OS-Reg" style="padding-bottom: 20px;">
                <div class="panel-group " id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                    <div class="row flex-row-center">
                                        <div class="col-xs-2">
                                            <img src="<?=base_url()?>public/images/OTC%20TRADING.svg" class="mobile-faq">
                                        </div>
                                        <div class="col-xs-10">
                                            <h3 class="accordion-title">About CoinSwitch</h3>
                                        </div>
                                    </div>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse">
                            <div class="panel-body">
                                <!-- Here we insert another nested accordion -->
                                <div class="panel-group" id="accordion1">
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion1"
                                                   href="#collapseInnerOne">
                                                    <p class="">What is CoinSwitch?</p>
                                                </a></h4>
                                        </div>
                                        <div id="collapseInnerOne" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                CoinSwitch is a cryptocurrency exchange providing the best exchange rate on transactions by aggregating all leading exchanges and comparing their rates in real time. We support over <b>100 cryptocurrencies</b> and more than <b>6000 pairs</b> for exchange.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion1"
                                                                       href="#collapseInnerTwo">
                                                <p>Why should I trust you?</p>
                                            </a></h4>
                                        </div>
                                        <div id="collapseInnerTwo" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                CoinSwitch is built on the most trusted exchanges in the world. We provide a seamless experience by integrating with reliable and secure exchanges and helping our customers get the best rates while ensuring their security.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion1"
                                                                       href="#collapseInnerThree">
                                                <p>Can the rates change during the transaction?</p>
                                            </a></h4>
                                        </div>
                                        <div id="collapseInnerThree" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                Unlike ordinary money, cryptocurrencies are highly volatile, so the rate may fluctuate. It may happen that the exchange rate which you see before the transaction is different (high or low) after transaction gets completed. In that case, the exchanged quantity which you receive may differ from what you see during the comparison.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion1"
                                                                       href="#collapseInnerFour">
                                                <p>What is our transaction fees?</p>
                                            </a></h4>
                                        </div>
                                        <div id="collapseInnerFour" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                <p>
                                                    Our fees depends on the exchange you select for your transaction. The conversion rate you see is inclusive of the fees.
                                                    <br/><strong>Note:</strong> Below fees does not include any amount charged by the exchange itself.
                                                </p>
                                                <br/>
                                                <div class="row" style="padding:0px;">
                                                    <div class="col-lg-4  col-sm-6 col-md-4 col-xs-12 col-lg-offset-4 col-sm-offset-3 col-md-offset-4" style="padding:0px;">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th>Exchange</th>
                                                                <th>Fees</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>Changelly</td>
                                                                <td>0%</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Shapeshift</td>
                                                                <td>0%</td>
                                                            </tr>
                                                            <tr>
                                                                <td>EverCoin</td>
                                                                <td>0%</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Blocktrades</td>
                                                                <td>0%</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Bittrex</td>
                                                                <td>0.50%</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Cryptopia</td>
                                                                <td>0.50%</td>
                                                            </tr>
                                                            <tr>
                                                                <td>CoinSwitch</td>
                                                                <td>0.60%</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Inner accordion ends here -->
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                    <div class="row flex-row-center">
                                        <div class="col-xs-2">
                                            <img src="<?=base_url()?>public/images/BITCOIN%20WALLET.svg" class="mobile-faq">
                                        </div>
                                        <div class="col-xs-10">
                                            <h3 class="accordion-title">About Wallets</h3>
                                        </div>
                                    </div>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="panel-body">
                                <!-- Here we insert another nested accordion -->
                                <div class="panel-group" id="accordion2">
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion2"
                                                   href="#secondInnerOne" class="click-highlight">
                                                    <p>What’s the wallet address?</p>
                                                </a></h4>
                                        </div>
                                        <div id="secondInnerOne" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                A wallet is a digital place for your coins to be stored. Each coin has a certain wallet provider. That could be official GUI-wallets, web wallets and other applications. The wallet address represents a randomly generated combination of digits and letters. CoinSwitch doesn’t provide wallet addresses and never stores user deposits. To exchange cryptocurrencies, you need to have a wallet address.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion2"
                                                                       href="#secondInnerTwo" class="click-highlight">
                                                <p>How do I get a wallet address?</p>
                                            </a></h4>
                                        </div>
                                        <div id="secondInnerTwo" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                Just figure out, what coin do you want to buy and seek for a stable wallet version. As a rule, each coin has its official wallet client. Note that each wallet has its unique address or a tag with a private key that is required to restore your wallet if lost. CoinSwitch never asks your private keys. Store them in a safe place and never show anyone. Once private keys are stolen, your wallet with all the coins will be lost forever.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion2"
                                                                       href="#secondInnerThree">
                                                <p>What's the recipient’s address?</p>
                                            </a></h4>
                                        </div>
                                        <div id="secondInnerThree" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                In each transaction, there is always a sender and a recipient. The recipient’s address is the wallet address to receive the money you want to buy. For example, if you want to buy Ethereum (ETH), in the field, you should specify the ETH wallet address. In general, the recipient’s wallet address is the address we send coins bought, once a transaction is finished.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion2"
                                                                       href="#secondInnerFour">
                                                <p>What is a hash?</p>
                                            </a></h4>
                                        </div>
                                        <div id="secondInnerFour" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                A hash (or the tx/transaction ID) is a unique address of your transaction in a blockchain. Mostly, the hash is a combination of digits and lower case (upper in Ripple) letters that represents a proof that money is sent. Whenever you make a payment, you receive a hash displayed in your wallet. Hash should be located in a blockchain. Our support team may ask you to provide a hash. Sometimes it’s required to find your payment and resolve your issues if any.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion2"
                                                                       href="#secondInnerFive">
                                                <p>Why is my wallet address recognized as invalid?</p>
                                            </a></h4>
                                        </div>
                                        <div id="secondInnerFive" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                CoinSwitch supports most of the wallets. If your wallet address is recognized as invalid, there is always a reason: You confused Dash (DASH) with Dashcoin DSH). These are two different coins with two different amounts. You confused Factom (FCT) with Fantomcoin (FCN). Different currencies as well. Typo or character missing. Make sure that the address you specify matches the actual address of your wallet.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Inner accordion ends here -->
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default ">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
                                    <div class="row flex-row-center">
                                        <div class="col-xs-2">
                                            <img src="<?=base_url()?>public/images/DECENTRALIZED_2.svg" class="mobile-faq">
                                        </div>
                                        <div class="col-xs-10">
                                            <h3 class="accordion-title">About Blockchains</h3>
                                        </div>
                                    </div>
                                </a>
                            </h4>
                        </div>

                        <div id="collapse3" class="panel-collapse collapse">
                            <div class="panel-body">
                                <!-- Here we insert another nested accordion -->
                                <div class="panel-group" id="accordion3">
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion3"
                                                   href="#ThirdInnerOne">
                                                    <p>What is a memo?</p>
                                                </a></h4>
                                        </div>
                                        <div id="ThirdInnerOne" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                <p>   <p>A memo is a combination of digits and letters required to assign STEEM to a certain Steem
                                                wallet user.
                                                Memos are used in Steem wallets and indicated right here.</p>
                                                <p>In the wallet, a memo should look like this</p><img
                                                    src="../s3-ap-south-1.amazonaws.com/crypto-exchange/placeholder/image3.png"/>
                                                <p>If you want to buy Steems, make sure that you've specified a correct memo while putting in a
                                                    wallet
                                                    address.</p>
                                                <p>Note that you have a nickname on steemit.com, you may use it as your wallet address that
                                                    doesn't require
                                                    a memo.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion3"
                                                                       href="#ThirdInnerTwo">
                                                <p>What is a destination tag?</p>
                                            </a></h4>
                                        </div>
                                        <div id="ThirdInnerTwo" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                <p>A destination tag is a short complementary combination of characters required for certain
                                                    Ripple wallets.
                                                    While buying Ripple, make sure that you have specified a destination tag if required.
                                                    Likewise, when you
                                                    send us Ripples, don't forget about a destination tag provided near the address to send
                                                    to.</p><img
                                                    src="../s3-ap-south-1.amazonaws.com/crypto-exchange/placeholder/image2.png"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion3"
                                                                       href="#ThirdInnerThree">
                                                <p>What is a message?</p>
                                            </a></h4>
                                        </div>
                                        <div id="ThirdInnerThree" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                <p>The message is an additional tag required for XEM transaction. Whenever you send or receive XEM,
                                                    make sure
                                                    that you've input a message. You may find it in your wallet, and it should look like
                                                    this.</p><img
                                                    src="../s3-ap-south-1.amazonaws.com/crypto-exchange/placeholder/image1.png"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Inner accordion ends here -->
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
                                    <div class="row flex-row-center">
                                        <div class="col-xs-2">
                                            <img src="<?=base_url()?>public/images/BITCOIN%20EXCHANGE.svg" class="mobile-faq">
                                        </div>
                                        <div class="col-xs-10">
                                            <h3 class="accordion-title">About Exchanging Cryptocurrency and altcoin</h3>
                                        </div>
                                    </div>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse">
                            <div class="panel-body">
                                <!-- Here we insert another nested accordion -->
                                <div class="panel-group" id="accordion4">
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion4"
                                                   href="#FourthInnerOne">
                                                    <p>How do I exchange coins on CoinSwitch?</p>
                                                </a></h4>
                                        </div>
                                        <div id="FourthInnerOne" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                <p>For all the coins available on CoinSwitch, the buying process is same.</p>
                                                <ol>
                                                    <li>Go to coinswitch.co and select the coin you want to exchange on the 'YOU HAVE' and the
                                                        coin you want
                                                        to buy on 'YOU GET' fields
                                                    </li>
                                                    <li>Enter the amount you want to exchange in 'YOU HAVE' field and click 'Continue!'</li>
                                                    <li>Check the amount specified and the amount you’re gonna get.</li>
                                                    <li>You will see the details of all the exchanges with the rates they are offering.</li>
                                                    <li>Click the 'Exchange' of the best exchange rate.</li>
                                                    <li>Specify the recipient’s wallet address.</li>
                                                    <li>Once again, check everything attentively. Note that the amount is estimated, so it may
                                                        be more or
                                                        less at the end of your transaction. If you’re OK with this, proceed to the next step.
                                                    </li>
                                                    <li>Now your transaction is created. You see a QR code and a wallet address below. This is
                                                        the address
                                                        you should send us your money to be exchanged. Go to the wallet from where you should
                                                        send money and
                                                        paste this address into a corresponding field. If you use a mobile wallet app, just scan
                                                        the QR.
                                                        Once we receive your money, we’ll exchange it and sent to the address you provided.
                                                    </li>
                                                    <li>It shows a progress bar with transaction status. Once it’s finished, you’ll be informed.
                                                        Usually, it
                                                        takes 15-30 minutes. In some cases, waiting time might be increased.
                                                    </li>
                                                    <li>After a transaction is finished, you get a receipt with an output transaction <a
                                                            href="#what-is-a-hash">hash</a> This hash is a proof that your transaction is
                                                        finished.
                                                    </li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion4"
                                                                       href="#FourthInnerTwo">
                                                <p>What is the minimum/maximum amount?</p>
                                            </a></h4>
                                        </div>
                                        <div id="FourthInnerTwo" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                <p>CoinSwitch doesn’t have amount restrictions. However, there are some nuances.</p>
                                                <p>If an amount is too low, make sure that it will cover all the <a
                                                        href="#what-are-network-fees">network
                                                    fees</a> a blockchain takes. It means that the amount you are going to send and the amount
                                                    you are going
                                                    to get should be enough to cover input and output network fees taken by a blockchain.</p>
                                                <p>If your funds to be exchanged are insufficient, you will see a message. <p/>
                                                <p>Please stick with this recommendations. Otherwise, there is a high risk that your money will
                                                    be lost.</p>
                                                <p>Some exchanges provide a max limit as well.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion4"
                                                                       href="#FourthInnerThree">
                                                <p>What are the network fees?</p>
                                            </a></h4>
                                        </div>
                                        <div id="FourthInnerThree" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                Every cryptocurrency is based on a blockchain. The process requires a network fee.
                                                This is a commission a blockchain takes from the amount you and we send. This is why it is
                                                so important to include network fees in the amount you send and going to get. If the amount
                                                is too low to cover the fees, a transaction will fail.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion4"
                                                                       href="#FourthInnerFour">
                                                <p>When should I receive my money?</p>
                                            </a></h4>
                                        </div>
                                        <div id="FourthInnerFour" class="panel-collapse collapse ">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                CoinSwitch transactions take 15-30 minutes to be processed. If a transaction is large (around 1 BTC worth), processing may take longer depending on the size of your transaction and blockchain capacity.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Inner accordion ends here -->
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
                                    <div class="row flex-row-center">
                                        <div class="col-xs-2">
                                            <img src="<?=base_url()?>public/images/TIMESTAMP.svg" class="mobile-faq">
                                        </div>
                                        <div class="col-xs-10">
                                            <h3 class="accordion-title">About Transactions</h3>
                                        </div>
                                    </div>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse5" class="panel-collapse collapse">
                            <div class="panel-body">
                                <!-- Here we insert another nested accordion -->

                                <div class="panel-group" id="accordion5">
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion5"
                                                   href="#FifthInnerOne">
                                                    <p>How do I cancel my transaction?</p>
                                                </a></h4>
                                        </div>
                                        <div id="FifthInnerOne" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                Blockchain transactions are irreversible. Once the money is sent, it cannot be rolled back. So if you are going to exchange cryptos, think twice and check attentively all the payment details prior to sending money.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion5"
                                                                       href="#FifthInnerTwo">
                                                <p>Why does my transaction take so long?</p>
                                            </a></h4>
                                        </div>
                                        <div id="FifthInnerTwo" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                <p>As a rule, our transaction takes 15-30 minutes to be processed. If your transaction takes
                                                    longer, this might be due to a wide range of factors.
                                                </p>
                                                <ol>
                                                    <li>Blockchain overloaded. There are many transactions pending including yours. These issues take
                                                        place on blockchains. All you have to do is to wait. CoinSwitch doesn't control blockchain delays.
                                                    </li>
                                                    <li>DDoS attacks. Any platform may experience such issues. In this case, all you have to do is
                                                        wait.</li>
                                                </ol>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion5"
                                                                       href="#FifthInnerThree">
                                                <p>Why is my transaction still waiting for payment if I've already
                                                    paid?</p>
                                            </a></h4>
                                        </div>
                                        <div id="FifthInnerThree" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                <p>This might be due to several reasons.</p>
                                                <ul style="padding-left: 15px;">
                                                    <li>The transaction hasn't been included to a blockchain. Cryptocurrencies aren't stable, so
                                                        minor errors
                                                        might occur. Either can we refund money or push a payment through if you provide the <a
                                                                href="#">hash </a>(a tx ID) of your transaction.
                                                    </li>
                                                    <li>ETC and ETH confusion. The addresses of Ethereum (ETH) and Ethereum Classic (ETH) are of
                                                        the same
                                                        structure. If you send ETC or ETH, make sure that you've created an appropriate
                                                        transaction on
                                                        CoinSwitch. For example, if you create an ETH to BTC transaction, make sure that you
                                                        send ETH, not
                                                        ETC. Otherwise, your transaction will be stuck.
                                                    </li>
                                                    <li>Wrong XEM <a href="#what-is-message">message</a>. While sending XEM, make sure that
                                                        you've put a
                                                        correct message. It's indicated here and looks like a combination of digits and letters.
                                                        Messages
                                                        like 'Hey how are you', 'I love CoinSwitch' etc. are lovely but don't work,
                                                        unfortunately :)
                                                    </li>
                                                    <li>Other internal errors. Even our perfect system may lag or experience internal issues. If
                                                        you suppose
                                                        that this is the case, report it to <a href="cdn-cgi/l/email-protection.html" class="__cf_email__" data-cfemail="5c2f292c2c332e281c3f3335322f2b35283f34723f33">[email&#160;protected]</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion5"
                                                                       href="#FifthInnerFour">
                                                <p>What will happen if I close the browser tab?</p>
                                            </a></h4>
                                        </div>
                                        <div id="FifthInnerFour" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                <p>If you have transferred the amount, then you can surely close the browser tab. Our system will automatically complete the transaction, and the amount will be deposited into your recipient wallet address. </br>Just keep the the <b>order-id</b> with you for any follow ups.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion5"
                                                                       href="#FifthInnerFive">
                                                <p>Why blockchain verification is taking long?</p>
                                            </a></h4>
                                        </div>
                                        <div id="FifthInnerFive" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                Blockchain is peer to peer network where transactions need to be confirmed by the network. This process can take from few minutes to several hours depending on the cryptocurrency and load on the network. <br/>
                                                CoinSwitch or its partners don't play any role in blockchain confirmations. Please be patient in case of delay. Your fund is safe and the exchange will proceed once our partner receives the deposits.
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default border-top-0 box-shadow-0">
                                        <div class="panel-heading">
                                            <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion5"
                                                                       href="#FifthInnerSix">
                                                <p>Sent funds to the wrong address. Can I recover my funds?</p>
                                            </a></h4>
                                        </div>
                                        <div id="FifthInnerSix" class="panel-collapse collapse">
                                            <div class="panel-body border-top-0 inner-accord-padding">
                                                Unfortunately not. Once funds are sent to a wrong address, there is nothing we can do to recover them. This is why it is extremely important to double check the address you enter while ordering.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Inner accordion ends here -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row flex-row-center" style="margin-top: 40px">
                    <div class="col-xs-2">
                        <img src="<?=base_url()?>public/images/PEER-TO-PEER.svg" class="faq-img">
                    </div>
                    <div class="col-xs-10">
                        <h3 style="font-size: 20px" class="font-OS-Bold">Your issue not listed here?</h3>
                    </div>
                </div>
                <p id="issue-text">If you have a problem or a question about our service, you can reach us at
                    <span class="font-OS-Bold"><a href="cdn-cgi/l/email-protection.html" class="__cf_email__" data-cfemail="94e7e1e4e4fbe6e0d4f7fbfdfae7e3fde0f7fcbaf7fb">[email&#160;protected]</a></span><br/><br/>
                    We usually respond to email requests within 12 hours.</p>

            </div>
        </div>
    </div>
    <div class="footer font">
        <div class="container padding">
            <div class="row">
                <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3 hidden-xs" style="text-align:left;">
                    <h3 class="footer-font-14 font-OS-Bold"><a class="font-white" href="index.html">Home</a></h3>
                    <h3 class="footer-font-14 font-OS-Bold"><a class="smoothscroll font-white" href="#">FAQ </a></h3>
                    <h3 class="footer-font-14 font-OS-Bold"><a class="font-white" href="#" target="_blank">Terms </a></h3>
                    
                    
                    <h3 class="footer-font-14 font-OS-Bold"><a class="font-white" href="signin.html">Login / Register</a></h3>
                </div>

                <div class="col-xs-12 visible-xs">
                    <div class="footer-font-14 font-OS-Reg">
                        <p><a class="font-white" href="index.html">Home </a> | <a class="font-white" href="#"> FAQ </a> | <a class="font-white" target="_blank" href="#">
                            Terms</a> | <a class="font-white" href="#">Login / Register</a></p>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                    <h3 class="footer-heading footer-font-14 font-OS-Bold">LOGO</h3>

                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                    <h3 class="footer-heading footer-font-14 font-OS-Bold"></h3>

                   
                </div>
            </div>
        </div>
    </div>
</div>

<script data-cfasync="false" src="cdn-cgi/scripts/d07b1474/cloudflare-static/email-decode.min.js"></script><script src="<?=base_url()?>public/js/jquery-2.1.3.mine86f.js?v=2018021514"></script>
<script src="<?=base_url()?>public/js/lib/bootstrap.mine86f.js?v=2018021514"></script>
<script src="<?=base_url()?>public/js/pluginse86f.js?v=2018021514"></script>
<script src="<?=base_url()?>public/js/homepagee86f.js?v=2018021514"></script>
<script src="<?=base_url()?>public/js/lib/le.mine86f.js?v=2018021514"></script>
<script>
    LE.init('c06b54a6-de95-46f4-96cb-38c17286758f');
    function trackJavaScriptError(e) {
        var ie = window.event || {},
                errMsg = e.message || ie.errorMessage;
        var errSrc = (e.filename || ie.errorUrl) + ': ' + (e.lineno || ie.errorLine);

        /*** SEND TO SERVER***/
        LE.log('JavaScript Error :: ' + errMsg + ' in: ' + errSrc);
    }
    /**
     * Cross-browser event listener
     */
    if (window.addEventListener) {
        window.addEventListener('error', trackJavaScriptError, false);
    } else if (window.attachEvent) {
        window.attachEvent('onerror', trackJavaScriptError);
    } else {
        window.onerror = trackJavaScriptError;
    }

    !function () {
        var t;
        if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0,
                t.methods = ["identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on"],
                t.factory = function (e) {
                    return function () {
                        var n;
                        return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
                    };
                }, t.methods.forEach(function (e) {
            t[e] = t.factory(e);
        }), t.load = function (t) {
            var e, n, o, i;
            e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"),
                    o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js",
                    n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
        });
    }();
    drift.SNIPPET_VERSION = '0.3.1';
    drift.config({
        enableWelcomeMessage: false,
    })

    var show_welcome_message = function() {
        if (window.is_inactive) {
            drift.api.showWelcomeMessage();
        }
    }

    // 

        if ($('#specsallA').is(':visible')) {
            // do your special stuff here
        }
    });

    if($(window).width() > 980) {
        <!-- Start of Async Drift Code -->
        drift.load('hzea56fkmf94');
        drift.on('ready', function (api) {
            // Open
            drift.on('awayMessage:open', function (event) {
                window.location.hash = "help";
            })
            drift.on('welcomeMessage:open', function (event) {
                window.location.hash = "help";
            })
//
//            drift.on('startConversation', function (event) {
//                window.location.hash = "help";
//            })

            // Close
            drift.on('awayMessage:close', function (event) {
                window.location.hash = '';
            })
            drift.on('welcomeMessage:close', function (event) {
                window.location.hash = '';
            })
            drift.on('sidebarClose', function (event) {
                window.location.hash = '';
                setTimeout(function() {
                    drift.api.hideWelcomeMessage();
                    drift.api.hideAwayMessage();
                }, 20);
            })

//            setTimeout(function() {
//                show_welcome_message();
//            }, 60000);
        })
    }
    VERSION = '2018021514';
</script>
<script>
$(document).ready(function(){
    $("#send_amount").keyup(function(){
        $("#get_amount").addClass('loading-input');
        var send_amount = $(this).val();
        $.ajax({
            type:"POST",
            url:"<?=base_url('index.php/exchange/get_exchange_amount')?>",
            data:{'amount':send_amount},
            success:function(response){
                $("#get_amount").val(response);
                $("#get_amount").removeClass('loading-input');
            }

        });
    });
});
</script>
</body>
</html>