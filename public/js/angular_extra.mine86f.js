if(is_prod == '1') {
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-100639244-1', 'auto');
    ga('send', 'pageview', "/exchange/select");

    <!-- Facebook Pixel Code -->
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1570069006632609', {
        em: 'care@coinswitch.co'
    });
    fbq('track', 'PageView');

    // mix panel
    if (window.mixpanel) {
        mixpanel.track("Exchange_View");
    }
}

//Start Mixpanel
(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,
        0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
    for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);
mixpanel.init("02c8ea110f5d7c6dcf9180c69ebdc9ce");

//end Mixpanel


// Drift:
!function () {
    var t;
    if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0,
            t.methods = ["identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on"],
            t.factory = function (e) {
                return function () {
                    var n;
                    return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
                };
            }, t.methods.forEach(function (e) {
        t[e] = t.factory(e);
    }), t.load = function (t) {
        var e, n, o, i;
        e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"),
                o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js",
                n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
    });
}();
drift.SNIPPET_VERSION = '0.3.1';
drift.config({
    enableWelcomeMessage: false,
})

drift.load('hzea56fkmf94');
//if(is_prod == '1' && $(window).width() > 980) {
//    <!-- Start of Async Drift Code -->
//}


// Initialize Firebase
var config = {
    apiKey: "AIzaSyDVGFmdbypMrmEKTjhjYTk6qMe2DdIOhCo",
    authDomain: "coinswitchemailpaslogin.firebaseapp.com",
    databaseURL: "https://coinswitchemailpaslogin.firebaseio.com",
    projectId: "coinswitchemailpaslogin",
    storageBucket: "coinswitchemailpaslogin.appspot.com",
    messagingSenderId: "477744724610"
};

if(window.mixpanel) {
    mixpanel.track("Best_Exchange", {
        "exchange": "",
        "to_currency":"",
        "from_currency": "",
        "amount":"",
        "expected_amount":"",
        "second_best":"",
        "third_best":"",
        "available_exchanges":[],
        "not_supported_exchanges":[],
        "diabled_exchanges":[],
    });
}

console.log("Initialzing firebase");
firebase.initializeApp(config);
firebase.auth().useDeviceLanguage();



