var lazyLoadModules = [{
    name: 'loginModule',
    files: ['public/js/accounts/login_controller.min.js?v=' + VERSION]
},{
      name: 'profileModule',
      files: ['public/js/accounts/profile_controller.min.js?v=' + VERSION]
},{
    name: 'exchangeFlowModule',
    files: ['public/js/exchange_flow/exchange_flow_controller.min.js?v=' + VERSION]
}];

angular.module("app", ["ui.router", 'oc.lazyLoad', "app.routes", "app.controllers", "app.services", "app.directives", 'ngAnimate', 'ngSanitize', 'ui.bootstrap', 'ui.select', "ngToast", "ngclipboard", "ngIntlTelInput"])
// angular.module("app", ["ui.router", 'ui.bootstrap', 'views',"app.routes",  "app.controllers", "app.services",  "app.directives", "ngAnimate"])
    .config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
        $ocLazyLoadProvider.config({
            debug:true,
            events:true,
            modules: lazyLoadModules
        });
    }])
    .config(function (ngIntlTelInputProvider) {
        ngIntlTelInputProvider.set(
            {
                initialCountry: 'in',
                preferredCountries:["us","in","gb"],
            }
        );

    })
    .config(['$httpProvider', function ($httpProvider) {
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
        $httpProvider.interceptors.push('httpRequestInterceptor');
    }])
    .config(['$qProvider', function($qProvider) {
        $qProvider.errorOnUnhandledRejections(false);
    }])
    .config(['ngToastProvider', function(ngToastProvider) {
        ngToastProvider.configure({
            animation: 'slide',
            additionalClasses: 'toast-themed-color'
        });
    }])
     .factory('SweetAlert', ['$window', function SweetAlert($window) {
        var $swal = $window;

        return {
          swal: swal
        }

        function swal(config) {
          return $swal.swal(config);
        }
      }])
    .constant('CONSTANTS', (function() {
        return {
            CHANGELLY: 'CHANGELLY',
            SHAPESHIFT: 'SHAPESHIFT',
            BLOCKTRADES: 'BLOCKTRADES',
            EVERCOIN: 'EVERCOIN',
            BLOCKCHAIN_VERIFICATION: 'BLOCKCHAIN_VERIFICATION',
            EXCHNAGING: 'EXCHNAGING',
            SENDING: 'SENDING',
            TRANSFERRING: 'TRANSFERRING',
            FAILED: 'FAILED',
            TX_S_COMPLETE: 'complete', // TX_S indicates transaction state on frontend
            TX_S_PENDING: 'pending',
            TX_S_INPROGRESS: 'inprogress',
            TX_NO_DEPOSIT: 'no_deposits',
            TX_I_AWAITING_CONFIRMATIONS: 'confirm_awaiting',
            TX_RECEIVED: 'received',
            TX_SENDING: 'sending',
            TX_COMPLETE: 'complete',
            TX_FAILED: 'failed',
            TX_TIMEOUT: 'timeout',
            TX_REFUNDED: 'refunded',
            ENABLED: 'ENABLED',
            DISABLED: 'DISABLED',
            LIMIT_BREACHED: 'LIMIT_BREACHED'
        }
    })())
    .config(function($provide) {
        $provide.decorator("$exceptionHandler", function ($delegate) {
            return function (exception, cause) {
                $delegate(exception, cause);
                try {
                    var errorMessage = exception.toString();
                    if(window.LE) {
                        var errorJson = angular.toJson({errorUrl: window.location.href, errorMessage: errorMessage, cause: ( cause || "" )});
                        LE.log('JavaScript Error :: '  + errorJson);
                    }
                } catch (loggingError) {

                }
            };
        })
    })
    .run(function() {
        if(window.LE) {
            LE.init('c06b54a6-de95-46f4-96cb-38c17286758f');
        }
    });
angular.module("app.routes", [])
    .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('');
        // $urlRouterProvider.otherwise('/dashboard');
        $urlRouterProvider.otherwise(function ($injector, $location) {
            var path = $location.path();
            var args = $location.search();

            var list = [];
            for (var key in args) {
                if (args.hasOwnProperty(key)) {
                    list.push(args[key]);
                }
            }
            
            if (path.startsWith("/!")) {
                $location.path("/app" + path.substring(2)).search(list);
            }

            var state = $injector.get('$state');
            state.go('exchange_flow', {}, {location: 'replace'});
            return $location.path();
        });

        $stateProvider
        // Root pages
            .state('login', {
                url: '/app/login?back',
                views: {
                    default: {
                        templateUrl: 'public/views/accounts/login.html?v=' + VERSION,
                        controller: 'LoginMainController',
                    },
                    'top-bar@login': {
                        templateUrl: 'public/views/common/top-bar.html?v=' + VERSION,
                        controller: 'TopBarController',
                    }
                },
                resolve: {
                    loadMyFiles:function($ocLazyLoad) {
                        return $ocLazyLoad.load('loginModule')
                    },
                    showLoginState: function(){return false;}
                }
            })
            .state('email_action', {
                url: '/app/auth/action?mode&oobCode&apiKey',
                views: {
                    default: {
                        templateUrl: 'public/views/accounts/auth-action.html?v=' + VERSION,
                        controller: 'AuthActionController',
                    },
                     'top-bar@email_action': {
                         templateUrl: 'public/views/common/top-bar.html?v=' + VERSION,
                         controller: 'TopBarController',
                     }
                },
                resolve: {
                    loadMyFiles:function($ocLazyLoad) {
                        return $ocLazyLoad.load('loginModule')
                    },
                    showLoginState: function(){return false;}
                }
            })
            .state('forgotpassword', {
                url: '/app/forgot-password',
                views: {
                    default: {
                        templateUrl: 'public/views/accounts/forgot-password.html?v=' + VERSION,
                        controller: 'ForgotPasswordMainController',
                    },
                    'top-bar@forgotpassword': {
                        templateUrl: 'public/views/common/top-bar.html?v=' + VERSION,
                        controller: 'TopBarController',
                    }
                },
                resolve: {
                    loadMyFiles:function($ocLazyLoad) {
                        return $ocLazyLoad.load('loginModule')
                    },
                    showLoginState: function(){return false;}
                }
            })
            .state('track_order', {
                url: '/app/track',
                views: {
                    default: {
                        templateUrl: 'public/views/exchange_flow/track-order.html?v=' + VERSION,
                        controller: 'TrackOrderController',
                    },
                    'top-bar@track_order': {
                        templateUrl: 'public/views/common/top-bar.html?v=' + VERSION,
                        controller: 'TopBarController',
                    }
                },
                resolve: {
                    loadMyFiles:function($ocLazyLoad) {
                        return $ocLazyLoad.load('exchangeFlowModule')
                    },
                    showLoginState: function(){return true;}
                }
            })
            .state('changepassword', {
                url: '/web/change_password?id',
                views: {
                    default: {
                        templateUrl: 'templates/login/change_password.html',
                        controller: 'ChangePasswordController'
                    }

                }
            })
            .state('register', {
                url: '/app/register',
                views: {
                    default: {
                        templateUrl: 'public/views/accounts/signup.html?v=' + VERSION,
                        controller: 'LoginMainController',
                    },
                    'top-bar@register': {
                        templateUrl: 'public/views/common/top-bar.html?v=' + VERSION,
                        controller: 'TopBarController',
                    }
                },
                resolve: {
                    loadMyFiles:function($ocLazyLoad) {
                        return $ocLazyLoad.load('loginModule')
                    },
                    showLoginState: function(){return false;}
                }
            })
            .state('logout', {
                url: '/app/logout',
                views: {
                    default: {
                        templateUrl: 'public/views/accounts/logout.html?v=' + VERSION,
                        controller: 'LogoutMainController',
                    }
                },
                resolve: {
                    loadMyFiles:function($ocLazyLoad) {
                        return $ocLazyLoad.load('loginModule')
                    }
                }
            })
            .state('exchange_flow', {
                url: '/app/exchange?from&to&amount',
                views: {
                    default: {
                        templateUrl: 'public/views/exchange_flow/form.html?v=' + VERSION,
                        controller: 'ExchangeFlowMainController',
                    },
                    'top-bar@exchange_flow': {
                        templateUrl: 'public/views/common/top-bar.html?v=' + VERSION,
                        controller: 'TopBarController',
                    }
                },
                resolve: {
                    loadMyFiles:function($ocLazyLoad) {
                        return $ocLazyLoad.load('exchangeFlowModule')
                    },
                    showLoginState: function(){return true;}
                }
            })
            .state('exchange_flow.amounts', {
                templateUrl: 'public/views/exchange_flow/amounts_v2.html?v=' + VERSION,
            })
            .state('exchange_flow.sendto', {
                templateUrl: 'public/views/exchange_flow/send_to_v2.html?v=' + VERSION,
            })
            .state('exchange_flow.sending', {
                url: '/transaction/:transactionId',
                controller: 'ExchangeFlowTransactionController',
                templateUrl: 'public/views/exchange_flow/sending_v2.html?v=' + VERSION,
            })

            .state('profile', {
                url: '/app/profile',
                views: {
                    default: {
                        templateUrl: 'public/views/accounts/profile.html?v=' + VERSION,
                        controller: 'ProfileController',
                    },
                    'top-bar@profile': {
                        templateUrl: 'public/views/common/top-bar.html?v=' + VERSION,
                        controller: 'TopBarController',
                    }
                },
                resolve: {
                    loadMyFiles:function($ocLazyLoad) {
                        return $ocLazyLoad.load('profileModule')
                    },
                    showLoginState: function(){return true;}
                }
            })
            .state('dashboard.target', {
                // url: '/audience',
                views: {
                    default: {
                        templateUrl: 'templates/dashboard/target_audience.html',
                        controller: 'TargetAudienceController'
                    }
                }
            })
            .state('dashboard.ads', {
                // url: '/audience',
                views: {
                    default: {
                        templateUrl: 'templates/dashboard/manage_ads.html',
                        // controller: 'TargetAudienceController'
                    }
                }
            })
            .state('dashboard.detailed-analysis', {
                // url: '/audience',
                views: {
                    detailed_analysis: {
                        templateUrl: 'templates/dashboard/target_audience.html',
                        controller: 'TargetAudienceController'
                    }
                }
            })

            .state('not_found', {
                url: '^/not-found',
                views: {
                    default: {
                        templateUrl: 'templates/common/not_found.html'
                    }
                }
            });
    });

var AppController = function ($scope, $log, $state, appService, CONSTANTS) {
    $scope.appService = appService;
    $scope.init = function (from_currency, to_currency, amount, is_prod, is_logged_in) {
        $scope.currency_list = window.currency_list;
        $scope.coin_mapping = window.coin_mapping;
        $scope.from_currency = from_currency;
        $scope.to_currency = to_currency;
        $scope.amount = amount;
        $scope.is_prod = is_prod;
        $scope.isLoggedIn = is_logged_in;
        console.log("isLoggedIn " + is_logged_in);
        $log.debug("AppController Initialized");
    };
    $scope.CONSTANTS = CONSTANTS;
};

var AccessDeniedController = function ($scope) {

};

var TopBarController = function($scope, showLoginState) {
    $scope.showLoginState = showLoginState;
}

angular.module("app.controllers", [])
    .controller("AppController", ["$scope", "$log", "$state", "appService", "CONSTANTS", AppController])
    .controller("AccessDeniedController", ["$scope", AccessDeniedController])
    .controller("TopBarController", ["$scope", "showLoginState", TopBarController]);


var app = angular.module("app.directives", [])
    .directive('ngEnter', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    scope.$apply(function () {
                        scope.$eval(attrs.ngEnter, {'event': event});
                    });

                    event.preventDefault();
                }
            });
        };
    })
    .filter( 'shortNumber', function() {
        return function( number ) {
            if ( number ) {
                abs = Math.abs( number );
                if ( abs >= Math.pow( 10, 12 ) ) {
// t    rillion
                    number = ( number / Math.pow( 10, 12 ) ).toFixed( 1 ) + "T";
                } else if ( abs < Math.pow( 10, 12 ) && abs >= Math.pow( 10, 9 ) ) {
// b    illion
                    number = ( number / Math.pow( 10, 9 ) ).toFixed( 1 ) + "B";
                } else if ( abs < Math.pow( 10, 9 ) && abs >= Math.pow( 10, 6 ) ) {
// m    illion
                    number = ( number / Math.pow( 10, 6 ) ).toFixed( 1 ) + "M";
                } else if ( abs < Math.pow( 10, 6 ) && abs >= Math.pow( 10, 3 ) ) {
// t    housand
                    number = ( number / Math.pow( 10, 3 ) ).toFixed( 1 ) + "K";
                }
                return number;
            }
        };
    })
    .filter('dropDigits', function() {
        return function(floatNum, decimalCount) {
            return String(floatNum)
                .split('.')
                .map(function (d, i) { return i ? d.substr(0, decimalCount) : d; })
                .join('.');
        };
    })
    .directive('onlyDigits', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attr, ctrl) {
                function inputValue(val) {
                    if (val) {
                        var digits = val.replace(/[^0-9.]/g, '');

                        if (digits !== val) {
                            ctrl.$setViewValue(digits);
                            ctrl.$render();
                        }
                        return parseFloat(digits);
                    }
                    return "";
                }

                ctrl.$parsers.push(inputValue);
            }
        };
    })
    .directive('inProgressSpinner', function($parse) {
        return {
            restrict: 'A',
            transclude: true,
            scope: {
            },
            template: '<span ng-show="disabled"><i class="glyphicon glyphicon-refresh spinning"></i></span> <span ng-transclude></span>',
            link: function (scope, elem, attrs) {
                var disabled = $parse(attrs.ngDisabled);
                scope.disabled = false;
                scope.$watch(
                    function() {
                        return disabled(scope.$parent);
                    },
                    function(newval) {
                        scope.disabled = newval;
                    }
                );
            }
        };
    })
var HttpService = function ($http, $injector) {
    this.$http = $http;
    this.success = function (callback) {
        return function (r) {
            console.log("API call success");
            if (callback) {
                callback(r);
            }
        }
    };

    this.error = function (callback) {
        return function (r) {
            console.log("API call error. code: " + r.status + ", message: " + r.statusText);
            console.log(r.data);
            if (callback) {
                callback(r);
            } else {
                $injector.get('ngToast').create({
                    className: 'danger',
                    content: 'Some error occurred',
                    timeout: 3000
                });
            }
        }
    };
    this.getAPIEndPoint = function () {
        return "";
    };
};

var HttpRequestInterceptor = function ($q, $location, $injector, $log, appService) {
    this.request = function (config) {
        $log.debug("Http call: " + config.method + " " + config.url);
        appService.showLoader();
        return config;
    };

    this.requestError = function (response) {
        $log.debug("Http request error: " + response.config.method + " " + response.config.url);
        appService.hideLoader();
        return $q.reject(response);
    };

    this.response = function (response) {
        $log.debug("Http response: " + response.config.method + " " + response.config.url);
        appService.hideLoader();
        return response;
    };

    this.responseError = function (response) {
        $log.debug("Http response error: " + response.config.method + " " + response.config.url + ", " + response.status);
        appService.hideLoader();
        if (response.status === 401) {
            window.location = "/login?next=" + $location.absUrl();
        }
        return $q.reject(response);
    };
};

var AppService = function ($log) {
    this.title = "Welcome to Launchpad";
    this.header = "";
    this.loader = false;
    this.loaderEnabled = true;
    this.loaderCounter = 0;
    this.createdByID = 0;

    this.getTitle = function () {
        return this.title;
    };

    this.setTitle = function (title) {
        this.title = title;
    };

    this.getCreatedByID = function () {
        return this.createdByID;
    };

    this.setCreatedByID = function (createdByID) {
        this.createdByID = createdByID;
    };

    this.getHeader = function () {
        return this.header;
    };

    this.setHeader = function (header) {
        this.header = header;
    };

    this.shouldShowLoader = function () {
        return this.loader;
    };

    this.showLoader = function () {
        this.loaderCounter++;

        if (!this.loaderEnabled) {
            $log.debug("Loader is disabled");
            this.loader = false;
            return;
        }

        if (this.loaderCounter > 0) {
            this.loader = true;
            $log.debug("Showing loader [Counter: " + this.loaderCounter + "]");
        } else {
            $log.debug("Not showing loader [Counter: " + this.loaderCounter + "]");
        }
    };

    this.hideLoader = function () {
        this.loaderCounter--;

        if (!this.loaderEnabled) {
            $log.debug("Loader is disabled");
            this.loader = false;
            return;
        }

        if (this.loaderCounter <= 0) {
            this.loader = false;
            $log.debug("Hiding loader [Counter: " + this.loaderCounter + "]");
        } else {
            $log.debug("Not hiding loader [Counter: " + this.loaderCounter + "]");
        }
    };

    this.disableLoader = function () {
        $log.debug("Disabling loader");
        this.loaderEnabled = false;
    };

    this.enableLoader = function () {
        $log.debug("Enabling loader");
        this.loaderEnabled = true;
    }
};

var CoinExchangeService = function ($http, $injector, $log) {
    HttpService.call(this, $http, $injector, $log);

    this.getIpDetails = function() {
        var url = this.getAPIEndPoint() + "/get_my_ip";
        return this.$http.get(url);
    }

    this.getExchangeAmount = function (body, exchanges) {
        var request_body = {
            "body": body
        };

        if(exchanges && exchanges.length > 0) {
            request_body["exchanges"] = exchanges;
        }

        var url = this.getAPIEndPoint() + "/api/v1/get-exchange-amount";
        return this.$http.post(url, request_body);
    };

    this.isExchangeAllowed = function(exchange) {
        var request_body = {
            "exchange": exchange
        };

        var url = this.getAPIEndPoint() + "/api/v1/is-exchange-allowed";
        return this.$http.post(url, request_body);
    }

    this.createTransaction = function(body, exchange) {
        var request_body = {
            "body": body
        };

        if(exchange) {
            request_body["exchange"] = exchange;
        }

        var url = this.getAPIEndPoint() + "/api/v1/create-transaction";
        return this.$http.post(url, request_body);
    }

    this.getTransactionStatus = function(transactionId, sync) {
        if(sync === undefined) {
            sync = true;
        }
        var url = this.getAPIEndPoint() + "/api/v1/transaction-status/" + transactionId + "?sync=" + sync;
        return this.$http.get(url);
    }

    this.doesTransactionExist = function(transactionId) {
        var url = this.getAPIEndPoint() + "/api/v1/transaction-status/" + transactionId + "/exists";
        return this.$http.get(url);
    }

    this.isAddressValid = function (addressId, coinSymbol, exchange) {
        var url = this.getAPIEndPoint() + "/api/v1/validate-address/" + addressId + "/" + coinSymbol + "?exchange=" + exchange;
        return this.$http.get(url);
    }

    this.getWarnings = function() {
        var url = this.getAPIEndPoint() + "/api/v1/warning";
        return this.$http.get(url);
    }

};

var ValidationService = function ($log) {
    var me = this;

    // ng-pattern, required etc angular directives lead to this function with parameters form object, name of the field, type of input.
    me.validateInput = function (form, name, type) {
        if(!form){
            return false;
        }
        var input = form[name];
        if (!input) {
            return false;
        }
        return (input.$dirty || form.$submitted) && input.$error[type];
    };

};


var UserService = function ($http, $injector, $log) {
    HttpService.call(this, $http, $injector, $log);
    this.validateFireBaseToken = function(idToken) {
        var request_body = {
            "token": idToken
        };
        var url = this.getAPIEndPoint() + "/api/v1/firebase-auth";
        return this.$http.post(url, request_body);
    }

    this.syncEmailVerification = function() {
        var url = this.getAPIEndPoint() + "/api/v1/me/sync-email-verification";
        return this.$http.put(url);
    }

    this.signUpBasedOnFirebaseToken = function(idToken) {
        var body = {
            "token": idToken
        };
        var url = this.getAPIEndPoint() + "/api/v1/signup-firebase-token";
        return this.$http.post(url, body);
    }

    this.signInBasedOnFirebaseToken = function(idToken) {
        var body = {
            "token": idToken
        };
        var url = this.getAPIEndPoint() + "/api/v1/signin";
        return this.$http.post(url, body);
    }

    this.getMe = function() {
        var url = this.getAPIEndPoint() + "/api/v1/me";
        return this.$http.get(url);
    }

    this.update = function(user) {
        var url = this.getAPIEndPoint() + "/api/v1/user-account";
        return this.$http.put(url, user);
    }

    this.sendVerificationEmail = function(user) {
        var url = this.getAPIEndPoint() + "/api/v1/auth/send-verify-email";
        return this.$http.post(url);
    }

    this.verifyEmailCustomVerifCode = function(verificationCode) {
        var url = this.getAPIEndPoint() + "/api/auth/verify-email/" + verificationCode;
        return this.$http.get(url);
    }

    this.updatePhoneFromFirebaseToken = function(idToken) {
        var request_body = {
          "token": idToken
        };
        var url = this.getAPIEndPoint() + "/api/v1/update-phone";
        return this.$http.put(url, request_body);
    }

    this.logout = function() {
        var url = this.getAPIEndPoint() + "/api/v1/signout";
        return this.$http.get(url);
    }
};

var CoinPriceUSDService = function ($http, $injector, $log) {
    HttpService.call(this, $http, $injector, $log);

    this.getDollarValue = function (cmcCoinId) {
        var url = "https://api.coinmarketcap.com/v1/ticker/"+cmcCoinId+"/?convert=USD";
        return this.$http.get(url);
    };
};

var AnchorSmoothScroll = function(){

    this.scrollTo = function(eID, correctionY) {

        // This scrolling function
        // is from http://www.itnewb.com/tutorial/Creating-the-Smooth-Scroll-Effect-with-JavaScript

        var startY = currentYPosition();
        var stopY = elmYPosition(eID);
        if (stopY) {
            stopY = stopY - correctionY;
        }
        var distance = stopY > startY ? stopY - startY : startY - stopY;
        if (distance < 100) {
            scrollTo(0, stopY); return;
        }
        var speed = Math.round(distance / 100);
        if (speed >= 20) speed = 20;
        var step = Math.round(distance / 25);
        var leapY = stopY > startY ? startY + step : startY - step;
        var timer = 0;
        if (stopY > startY) {
            for ( var i=startY; i<stopY; i+=step ) {
                setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
                leapY += step; if (leapY > stopY) leapY = stopY; timer++;
            } return;
        }
        for ( var i=startY; i>stopY; i-=step ) {
            setTimeout("window.scrollTo(0, "+leapY+")", timer * speed);
            leapY -= step; if (leapY < stopY) leapY = stopY; timer++;
        }

        function currentYPosition() {
            // Firefox, Chrome, Opera, Safari
            if (self.pageYOffset) return self.pageYOffset;
            // Internet Explorer 6 - standards mode
            if (document.documentElement && document.documentElement.scrollTop)
                return document.documentElement.scrollTop;
            // Internet Explorer 6, 7 and 8
            if (document.body.scrollTop) return document.body.scrollTop;
            return 0;
        }

        function elmYPosition(eID) {
            var elm = document.getElementById(eID);
            var y = elm.offsetTop;
            var node = elm;
            while (node.offsetParent && node.offsetParent != document.body) {
                node = node.offsetParent;
                y += node.offsetTop;
            } return y;
        }

    };
};


angular.module("app.services", [])
    .service("coinExchangeService", ["$http","$injector", "$log", CoinExchangeService])
    .service('httpRequestInterceptor', ["$q", "$location", "$injector", "$log", "appService", HttpRequestInterceptor])
    .service("appService", ["$log", AppService])
    .service("validationService", ["$log", ValidationService])
    .service("userService", ["$http","$injector", "$log", UserService])
    .service("coinPriceUSDService", ["$http","$injector", "$log", CoinPriceUSDService])
    .service("anchorSmoothScroll", [AnchorSmoothScroll]);

